import timeit

test_code = '''
from timeseries_store import TimeseriesStore

ts = TimeseriesStore()

# Load and store data from CSV file directly in the class instance
ts.store_dataset_from_file_under_key("../csv/series0.csv", "key1")
print("First series")
print(ts.get_dataset_under_key("key1"))

# Load and store data from CSV file using the utility method
df = TimeseriesStore.load_dataset("../csv/series1.csv")
ts.store_dataset_under_key(df, "key1")
print("Second series")
print(ts.get_dataset_under_key("key1"))

# Load and store data from CSV file directly in the class instance
ts.store_dataset_from_file_under_key("../csv/series2.csv", "key1")
print("Third series")
print(ts.get_dataset_under_key("key1"))

print("Data for specified time range")
print(ts.get_timeseries_under_key_within_timerange("2018-01-01 12:00:00", "2018-01-13 12:00:00", "key1"))

missing_periods = ts.get_missing_periods_under_key_within_timerange("2018-01-08 04:00:00", "2018-01-17 09:00:00",
                                                                    "key1")
print("Missing periods")
print(missing_periods)

print("Data deletion")
ts.delete_timeseries_under_key("key1")
print(ts.get_dataset_under_key("key1"))
'''

no_of_exec = 1
print(
    f"Avg execution time for {no_of_exec} trial(s): {timeit.timeit(stmt=test_code, number=no_of_exec) / no_of_exec}s")
