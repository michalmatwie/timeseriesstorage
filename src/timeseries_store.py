import pandas as pd
from itertools import groupby


class TimeseriesStore(object):
    """
    A class used to store time series data

    ...

    Attributes
    ----------
    stored_timeseries : (dict of str: pandas.DataFrame)
        A dictionary used to store time series data

    Methods
    -------
    load_dataset(path_to_dataset)
        Loads and returns the data from a CSV file from given path under the given key. Static method that can be used
        as a utility function independently from storing the data.
    store_dataset_under_key(self, dataset, key)
        Stores data from the passed dataframe under the key.
    store_dataset_from_file_under_key(path_to_dataset, key)
        Loads and stores data from the given path under the key.
    print_dataset_under_key(key, n=None)
        Prints the data to the console.
    print_timeseries_under_key_within_timerange(start, end, key)
        Prints the data under the key for the given time interval.
    get_missing_periods_under_key_within_timerange(start, end, key, freq="H")
        Returns an array of periods for which there is not data in the dataframe under the given key.
        Precision of the search can be adjusted by the freq parameter.
    delete_timeseries_under_key(key)
        Deletes the data under the given key from the dictionary.
    """
    def __init__(self):
        self.stored_timeseries = {}

    @staticmethod
    def load_dataset(path_to_dataset):
        '''
        Loads and returns the data from a CSV file from given path under the given key.
        The file should contain two columns:
            (1) datetime that can be parsed by pandas as datetime
            (2) value that can be prased by pandas as float
        Data is loaded into a pandas.DataFrame that is indexed with pandas.DatetimeIndex build on top of the first
        column.

        Parameters
        ----------
        path_to_dataset : str
            Path to the data CSV file.

        Returns
        -------
        pandas.DataFrame indexed with pandas.DatetimeIndex with column 'value' of type float
        '''
        try:
            df = pd.read_csv(path_to_dataset, names=["datetime", "value"], header=0, dtype={"value": float},
                             index_col="datetime", parse_dates=True)
            return df
        except NameError:
            print("File is missing.")
        except Exception as e:
            print(f'Error in loading {path_to_dataset}')
            print(e)
        return pd.DataFrame(columns=["value"], index=pd.DatetimeIndex(data=[], freq="H"), dtype=float)

    def __is_dataset_valid(self, dataset):
        if isinstance(dataset, pd.DataFrame):
            column_is_valid = len(dataset.columns) == 1 and "value" in dataset.columns and dataset.value.dtype == float
            index_is_valid = isinstance(dataset.index, pd.DatetimeIndex)
            return column_is_valid and index_is_valid
        return False

    def store_dataset_under_key(self, dataset, key):
        '''
        Stores dataset under given key. If a dataframe is already being stored under the given key, the new passed
        dataframe will be merged with the old one overwriting rows with the same index.

        Parameters
        ----------
        dataset : pandas.DataFrame
            pandas.DataFrame indexed with pandas.DatetimeIndex with column 'value' of type float
        key : str
            A key that the data will be stored under in the stored_timeseries dictionary

        Raises
        ------
        TypeError when passed dataframe does not meet documented requirements
        '''
        if self.__is_dataset_valid(dataset):
            if key in self.stored_timeseries:
                self.stored_timeseries[key] = dataset.combine_first(self.stored_timeseries[key])
            else:
                self.stored_timeseries[key] = dataset
        else:
            raise TypeError('''dataset should be a pd.DataFrame indexed with pd.DatetimeIndex with one column named 
            'value' of type float''')

    def store_dataset_from_file_under_key(self, path_to_dataset, key):
        '''
        Loads and stores the data from a CSV file from given path under the given key.
        The file should contain two columns:
            (1) datetime that can be parsed by pandas as datetime
            (2) value that can be prased by pandas as float
        Data is loaded into a pandas.DataFrame that is indexed with pandas.DatetimeIndex build on top of the first
        column. If a dataframe is already being stored under the given key, the newly loaded dataframe will be merged
        with the old one overwriting rows with the same index.

        Parameters
        ----------
        path_to_dataset : str
            Path to the data CSV file.
        key : str
            A key that the data will be stored under in the stored_timeseries dictionary
        '''
        df = TimeseriesStore.load_dataset(path_to_dataset)
        self.store_dataset_under_key(df, key)

    def get_dataset_under_key(self, key, n=None):
        '''
        Returns the dataframe under the given key. When n is provided it returns top n rows, else it returns the entire
        dataframe. If there is no dataframe under the given key the function prints 'No dataset under given key' and
        returns None

        Parameters
        ----------
        key : str
            Key of the dataframe to be printed
        n : int, optional
            Used to define number of rows to be printed. When n=None prints the entire dataframe

        Returns
        -------
        DataFrame if data exists under given key
        None otherwise
        '''
        if key in self.stored_timeseries:
            return self.stored_timeseries[key] if n is None else self.stored_timeseries[key].head(n)
        else:
            print('No dataset under given key')

    def get_timeseries_under_key_within_timerange(self, start, end, key):
        '''
        Returns all the data from the dataframe under the given key for the specified time interval. If no data under
        the given key returns None. If no data in the given timerange returns empty DataFrame

        Parameters
        ----------
        start : any python object that can be prased as datetime by pandas
            Defines the start date of the interval
        end : any python object that can be prased as datetime by pandas
            Defines the end date of the interval
        key : str
            Key of the dataframe to be searched against

        Returns
        -------
        DataFrame if data exists under given key
        None otherwise
        '''
        if key in self.stored_timeseries:
            return self.stored_timeseries[key].loc[start: end]
        else:
            print('No dataset under given key')

    def get_missing_periods_under_key_within_timerange(self, start, end, key, freq="H"):
        '''
        Returns an array of periods for which there is no data in the specified time interval. The precision is better
        with the growth of frequency. Assumption: datetimes in the dataframe, start and end have have full hours and
        come with a frequency of N hours where N is an integer. To deal with exceptions from that assumption set the
        freq parameter to "T".

        Parameters
        ----------
        start : any python object that can be prased as datetime by pandas
            Defines the start date of the interval
        end : any python object that can be prased as datetime by pandas
            Defines the end date of the interval
        key : str
            Key of the dataframe to be searched against
        freq : pandas offset aliases, optional
            Used to define granularity of the search

        Returns
        -------
        Array [(star_of_missing_period, end_of_missing_period)] of missing periods
        or None if there is no dataframe under the given key
        '''
        if key in self.stored_timeseries:
            rng_idx = pd.Index(pd.date_range(start, end, freq=freq))
            missing_values = rng_idx.isin(self.stored_timeseries[key].index)
            current_index = 0
            missing_periods = []
            for presence, g in groupby(missing_values):
                group_length = len(list(g))
                if not presence:
                    missing_periods.append((rng_idx[current_index], rng_idx[current_index + group_length - 1]))
                current_index += group_length
            return missing_periods
        else:
            print('No dataset under given key')

    def delete_timeseries_under_key(self, key):
        '''
        Deletes the data from the given key and the key itself
        '''
        if key in self.stored_timeseries:
            del self.stored_timeseries[key]
        self.stored_timeseries.pop(key, None)
